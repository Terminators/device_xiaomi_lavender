export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache
ccache -M 100G
export CCACHE_COMPRESS=1
export SKIP_ABI_CHECKS=true
export SELINUX_IGNORE_NEVERALLOWS=true
export TEMPORARY_DISABLE_PATH_RESTRICTIONS=true

# Clone Proton Clang
git clone --depth=1 https://github.com/kdrag0n/proton-clang prebuilts/clang/host/linux-x86/clang-proton
